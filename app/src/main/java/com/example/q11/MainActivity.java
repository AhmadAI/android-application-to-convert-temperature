package com.example.q11;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q11.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn1;
    EditText etext1;
    Spinner drop1;
    TextView text3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = findViewById(R.id.btn1);
        etext1 = findViewById(R.id.etext1);
        drop1 = findViewById(R.id.drop1);
        text3 = findViewById(R.id.text3);

        String[] temp = {"Degree celsius","Degree fahrenheit"};
        ArrayAdapter ad = new ArrayAdapter<String>(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,temp);
        drop1.setAdapter(ad);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double temp = Double.parseDouble(etext1.getText().toString());
                if (drop1.getSelectedItem().toString()=="Degree celsius")
                {

                    double fah = (temp*9)/5+32;

                    text3.setText("The temperature in Degree fahrenheit is: "+fah+"F");
                }

                else if (drop1.getSelectedItem().toString()=="Degree fahrenheit")
                {

                    double cels = (temp-32)*5/9;

                    text3.setText("The temperature in Degree celsius is: "+cels+"C");
                }
            }
        });



    }
}